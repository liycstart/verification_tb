//===============================================================
//Copyright (c): ycli ALL rights reserved. 
//  Create by:
//      Email:
//       Date:
//   Filename:
//Description:
//    Version:
//Last Change:
//                                                                 
//===============================================================
                                                                 
`ifndef TB_TOP_INTERFACE__SV
`define TB_TOP_INTERFACE__SV

interface tb_top_interface(input logic clk,input logic rst_n);

    ahb_interface  AHB_INF (clk);
    apb_interface  APB_INF (clk);

endinterface:tb_top_interface

`endif //TB_TOP_INTERFACE__SV
